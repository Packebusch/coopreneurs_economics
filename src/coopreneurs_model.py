from mesa import Agent, Model
from mesa.time import RandomActivation
from scipy.stats import invgamma
import random

output_unit = 1
learning_factor = 1.3
founding_threshold = 100





class Worker():
    # Class for individual Coop workers
    def __init__(self, initial_wealth,initial_effectivity, initial_venture, coop):
        self.wealth = initial_wealth
        self.effectivity = initial_effectivity
        self.output =  self.effectivity * output_unit
        self.venture = initial_venture
        self.coop = coop
    
    def work(self):

        return (self.output)

    def change_venture(self):
        self.venture = random.choice(self.coop.get_ventures())
        self.learn(learning_factor)

    def learn(self, factor):
        self.effectivity *= factor
    
    def earn_money(self, income):
        self.wealth += income
        
    def current_wealth(self):
        return self.wealth

class Venture():
    def __init__(self, initial_capital, initial_workers, coops, vcs):
        self.capital = initial_capital
        # self.workers = initial_workers
        self.potential = random()
        self.coops = coops
        self.vcs = vcs
    
    def make_business():
        profit = 0 # Some probability distribution dependent
        return profit

class Coop(Agent):  
    # input: money
    # output: work
    # Initial assumption: Coop doesn't grow
    def __init__(self, unique_id, model, initial_workers, initial_capital, initial_ventures):
        super().__init__(unique_id, model)
        self.labour = initial_workers      
        self.capital = initial_capital
        self.ventures = initial_ventures

    def work(self):
        pass
    
    def get_ventures(self):
        # get all ventures the Coop is involved
        pass

    def new_venture(self):
        if self.capital > founding_threshold:
            venture = Venture()
            self.ventures.add(venture)
            self.capital -= founding_threshold

    Capital from investor to venture
    venture capital to coop
    coop gives work 
    work goes into profit 


    def step(self):
        # Coop exchanges work for capital
        # Capital comes from VCs and from Ventures
        # Work is done by all of the workers in one step. 
        # Workers are assigned to a venture
        # Workers can change ventures at every step
        # If they change ventures, they learn
        # Income is produced by ventures according to a probability distribution (which one?)
        # If capital of the coop is greater than a founding_threshold a new venture get's created.
        # Income is devided by all workers equally


        other_agent = self.random.choice(self.model.schedule.agents)
        other_agent.wealth += 1
        self.capital -= 1
    
    
class VC(Agent):
    def __init__(self, unique_id, model, initial_capital):
        self.capital = initial_capital
        self.ventures = []
    def invest():
        if self.capital > 0
            venture = random.choose(all_ventures_not_invested)
            self.ventures.add(venture)
            # investe random number < capital in the venture

         # choose venture to invest per random
    def earn():
        self.venture.get_dividend()
        # get dividend according to invest ratio

class VC(Agent):
    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.wealth = 1

    def step(self):
        self.model.schedule.agents
        if self.wealth == 0:
            return
        other_agent
        other_agent.wealth += 1
        self.wealth -= 1

class CoopreneursModel(Model):
    def __init__(self, N_coop, N_VC):
        self.schedule = RandomActivation(self)
        # Create agents
        for i in range(N_coop):
            a = CoopAgent(unique_id='Coop_' + str(i),model=self)
            self.schedule.add(a)
        for i in range(N_VC):
            a = VCAgent('VC_' + str(i), self)
            self.schedule.add(a)

    def step(self):
        '''Advance the model by one step.'''
        self.schedule.step()
