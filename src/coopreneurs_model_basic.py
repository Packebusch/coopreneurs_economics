from mesa import Agent, Model
from mesa.time import RandomActivation
import random

output_unit = 1
worker_growth = 3
all_vcs = []

# Coop is a agent. VC is only reacting, Coop creates ventures, gives work to a venture
# Venture is a agent. Venture produces money and pays dividends and money to coop
# Purpose Project is reacting. Recieves work, creates social value.
# 
# 

class Venture(Agent):
    def __init__(self, initial_capital_vc, initial_capital_coop, initial_workers, coops, vcs):
        self.capital_vc = initial_capital_vc
        self.capital_coop = initial_capital_coop
        self.coops = coops
        self.vcs = vcs
    
    def make_business():
        if random.random() < 0.1:
            # Verlust
        else:
            # Gewinn

class Coop(Agent):  
    def __init__(self, unique_id, model, initial_workers, initial_capital, initial_ventures):
        super().__init__(unique_id, model)
        self.labour = initial_workers      
        self.capital = initial_capital
        self.ventures = initial_ventures
  
    def new_venture(self):
        if self.capital > founding_threshold:
            venture = Venture(initial_capital_coop=founding_threshold, initial_capital_vc=0, initial_workers=worker_growth, coops=[self],vcs=[])
            self.labour += worker_growth
            self.ventures.add(venture)
            self.capital -= founding_threshold
        else:
            if random.random() > (founding_threshold - self.capital / founding_threshold):      # The odds, that the VC funds the venture are greater the less money he needs to invest.
                venture = Venture(initial_capital_vc=(founding_threshold - self.capital), initial_capital_coop=self.capital, initial_workers=worker_growth, coops=[self],vcs=[random.choice(all_vcs)])
                self.labour += worker_growth
                self.ventures.add(venture)
                self.capital = 0

    def step(self):
        
    
    
class VC():
    def __init__(self, unique_id, model, initial_capital):
        self.capital = initial_capital
        self.ventures = []
    
    def earn():
        self.venture.get_dividend()
        # get dividend according to invest ratio

class VC(Agent):
    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.wealth = 1

    def step(self):
        self.model.schedule.agents
        if self.wealth == 0:
            return
        other_agent
        other_agent.wealth += 1
        self.wealth -= 1

class CoopreneursModel(Model):
    def __init__(self, N_coop, N_VC):
        self.schedule = RandomActivation(self)
        # Create agents
        for i in range(N_coop):
            a = CoopAgent(unique_id='Coop_' + str(i),model=self)
            self.schedule.add(a)
        for i in range(N_VC):
            a = VCAgent('VC_' + str(i), self)
            self.schedule.add(a)

    def step(self):
        '''Advance the model by one step.'''
        self.schedule.step()
